#include <avr/io.h>
#include <util/delay.h>

int main(void) {
	// init IO
	DDRB |= (1<<PB1);	// PWM_A
	DDRB |= (1<<PB2);	// PWM_B

	PORTB |= (1<<PB1);
	PORTB &= ~(1<<PB2);

	while (1) {
        	PORTB |= (1<<PB1);
                _delay_ms(125);
                PORTB &= ~(1<<PB1);
                _delay_ms(125);
                _delay_ms(125);
                _delay_ms(125);
	}
}

