#include <avr/io.h>
#include <util/delay.h>

int main() {
	// init IO
	DDRB |= (1<<PB5);	// PWM_A
	DDRB |= (1<<PB7);	// PWM_B

	PORTB &= ~(1<<PB5);
	PORTB &= ~(1<<PB7);

	while (1) {
		PORTB |= (1<<PB5);
		PORTB &= ~(1<<PB5);
	}
}

