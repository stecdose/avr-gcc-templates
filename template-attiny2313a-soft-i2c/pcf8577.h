#ifndef _PCF8577_H
#define _PCF8577_H

/* pcf8577's LCD-RAM is 8 bytes, every bit is one LCD segment
 * and is being represented by corresponding bit in array
 * framebuffer[8]. This array can be updated and afterwards
 * synced to display.
 *
 * Also a timer that executes syncing 20...30 times per second
 * is sufficient, but implies data transfers even if there
 * isn't any update on screen.
 * On the other hand, this could save a lot programspace.
 */

/* The wiring of my display board is a mess, so I decided to
 * use some shifting bits and assembling bytes to save as much
 * memory as possible. The result are a few VERY small tables,
 * similar to old day's ROMs used for looking up addresses, 
 * seven segment decoding, etc....
 */


/* what follows is numbering scheme, don't change if adapting pin configuration, not needed to change. */
#define DIG0	0x00
#define DIG1	0x01
#define DIG2	0x03

#define SEG_A	0x01
#define SEG_B	0x02
#define SEG_C	0x04
#define SEG_D	0x08
#define SEG_E	0x10
#define SEG_F	0x20
#define SEG_G	0x40

#define ARRUP	0x00 	// arrow up (top left LCD corner)
#define TILDE	0x01 	//  
#define MINUS   0x02 	//  
#define M1		0x03	//
#define M2		0x04	//  
#define DOPPELP 0x05	//  
#define PH		0x06	//  
#define MV		0x07	//  
#define MICROS	0x08	//  
#define MILLIS	0x09	//	
#define PERCENT	0x0A	//	
#define CDEG	0x0B	//	
#define MOHM	0x0C	//	
#define MGperL	0x0D	//


/* here we set up specific bit addresses in framebuffer-array */
// array indexes (i) and bit numbers (b)
/*
 *
 *
 *
 *
 */

#define D0_Ai		0x05
#define D0_Ab		0x01
#define D0_Bi		0x03
#define D0_Bb		0x02
#define D0_Ci		0x03
#define D0_Cb		0x00
#define D0_Di		0x02
#define D0_Db		0x00
#define D0_Ei		0x00
#define D0_Eb		0x07
#define D0_Fi		0x04
#define D0_Fb		0x01
#define D0_Gi		0x01
#define D0_Gb		0x07

#define D1_Ai		0x05
#define D1_Ab		0x03
#define D1_Bi		0x05
#define D1_Bb		0x02
#define D1_Ci		0x01
#define D1_Cb		0x06
#define D1_Di		0x00
#define D1_Db		0x06
#define D1_Ei		0x00
#define D1_Eb		0x05
#define D1_Fi		0x04
#define D1_Fb		0x03
#define D1_Gi		0x01
#define D1_Gb		0x05

#define D2_Ai		0x05
#define D2_Ab		0x06
#define D2_Bi		0x05
#define D2_Bb		0x05
#define D2_Ci		0x01
#define D2_Cb		0x04
#define D2_Di		0x00
#define D2_Db		0x04
#define D2_Ei		0x00
#define D2_Eb		0x03
#define D2_Fi		0x04
#define D2_Fb		0x06
#define D2_Gi		0x01
#define D2_Gb		0x03

#define ARRUPi		0x07 	//  arrow up (top left LCD corner), byte 7, bit 1
#define ARRUPb		0x01 	//  arrow up (top left LCD corner), byte 7, bit 1
#define TILDEi		0x06 	//  6/0
#define TILDEb		0x00  	//  6/0
#define MINUSi   	0x06 	//  6/1
#define MINUSb  	0x01 	//  6/1
#define M1i			0x01	//	1/1
#define M1b			0x01	//	1/1
#define M2i			0x00	//  0/1
#define M2b			0x01	//  0/1
#define DOPPELPi 	0x05	//  5/4
#define DOPPELPb 	0x04	//  5/4
#define PHi			0x03	//  3/6
#define PHb			0x06	//  3/6
#define MVi			0x03	//  3/4
#define MVb			0x04	//  3/4
#define MICROSi		0x03	//  3/5
#define MICROSb		0x05	//  3/5
#define MILLISi		0x03	//	3/3
#define MILLISb		0x03	//	3/3
#define PERCENTi	0x02	//	2/5
#define PERCENTb	0x05	//	2/5
#define CDEGi		0x02	//	2/3
#define CDEGb		0x03	//	2/3
#define MOHMi		0x02	//	2/4
#define MOHMb		0x04	//	2/4
#define MGperLi		0x02	//	2/2
#define MGperLb		0x02	//	2/2

#define NUM_0   SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F
#define NUM_1   SEG_B|SEG_C
#define NUM_2   SEG_A|SEG_B|SEG_D|SEG_E|SEG_G
#define NUM_3   SEG_A|SEG_B|SEG_C|SEG_D|SEG_G
#define NUM_4   SEG_B|SEG_C|SEG_F|SEG_G
#define NUM_5   SEG_A|SEG_F|SEG_G|SEG_C|SEG_D
#define NUM_6   SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_7   SEG_A|SEG_B|SEG_C
#define NUM_8   SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_9 	SEG_A|SEG_B|SEG_C|SEG_D|SEG_F|SEG_G
#define NUM_A	SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G
#define NUM_B	SEG_D|SEG_C|SEG_E|SEG_F|SEG_G
#define NUM_C	SEG_G|SEG_D|SEG_E
#define NUM_D	SEG_B|SEG_C|SEG_D|SEG_E|SEG_G
#define NUM_E	SEG_A|SEG_D|SEG_E|SEG_F|SEG_G
#define NUM_F	SEG_A|SEG_E|SEG_F|SEG_G

#define CODEB_0 	NUM_0
#define CODEB_1 	NUM_1
#define CODEB_2 	NUM_2
#define CODEB_3 	NUM_3
#define CODEB_4		NUM_4
#define CODEB_5 	NUM_5
#define CODEB_6		NUM_6
#define CODEB_7		NUM_7
#define CODEB_8		NUM_8
#define CODEB_9		NUM_9
#define CODEB_MINUS	SEG_G
#define CODEB_H		SEG_B|SEG_C|SEG_E|SEG_F|SEG_G
#define CODEB_E		NUM_E
#define CODEB_L		SEG_D|SEG_E|SEG_F
#define CODEB_P		SEG_A|SEG_B|SEG_E|SEG_F|SEG_G

#define LTR_UPPER_I		SEG_E|SEG_F
#define LTR_LOWER_R		SEG_E|SEG_G


void pcf8577_init(void);
void pcf8577_syncfb(void);
void pcf8577_ss_putint(int i);
void pcf8577_clr_ss(void);
void pcf8577_clr_symbols(void);
void pcf8577_clr_dp(void);
void pcf8577_setram(uint8_t pattern);
#define pcf8577_clr() pcf8577_setram(0x00)
#define pcf8577_fill() pcf8577_setram(0xff)

void pcf8577_set_sym(uint8_t n);
void pcf8577_set_dp(uint8_t n);

void pcf8577_set_digit(uint8_t digit, uint8_t value);
void pcf8577_clr_digit(uint8_t n);

#endif
