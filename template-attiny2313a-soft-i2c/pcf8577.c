#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "i2cmaster.h"
#include "pcf8577.h" 

static uint8_t framebuffer[8];

static const uint8_t symtab[14] PROGMEM = {  ((  ARRUPi<<4)|ARRUPb),
					    ((  TILDEi<<4)|TILDEb),
					    ((  MINUSi<<4)|MINUSb),
					    ((     M1i<<4)|M1b),
						((     M2i<<4)|M2b),
						((DOPPELPi<<4)|DOPPELPb),
						((     PHi<<4)|PHb),
						((     MVi<<4)|MVb),
						(( MICROSi<<4)|MICROSb),
						(( MILLISi<<4)|MILLISb),
						((PERCENTi<<4)|PERCENTb),
						((   CDEGi<<4)|CDEGb),
						((   MOHMi<<4)|MOHMb),
						(( MGperLi<<4)|MGperLb)
};
         
static const uint8_t dptab[3] PROGMEM = { ((0<<4)|2), ((4<<4)|5), ((4<<4)|2) };

static const uint8_t segtab[3][7] PROGMEM = {  { ((D0_Ai<<4)|D0_Ab),
							((D0_Bi<<4)|D0_Bb),
							((D0_Ci<<4)|D0_Cb),
							((D0_Di<<4)|D0_Db),
							((D0_Ei<<4)|D0_Eb),
							((D0_Fi<<4)|D0_Fb),
							((D0_Gi<<4)|D0_Gb)
						   },
						   {((D1_Ai<<4)|D1_Ab),
							((D1_Bi<<4)|D1_Bb),
							((D1_Ci<<4)|D1_Cb),
							((D1_Di<<4)|D1_Db),
							((D1_Ei<<4)|D1_Eb),
							((D1_Fi<<4)|D1_Fb),
							((D1_Gi<<4)|D1_Gb)
						   },
						   {((D2_Ai<<4)|D2_Ab),
							((D2_Bi<<4)|D2_Bb),
							((D2_Ci<<4)|D2_Cb),
							((D2_Di<<4)|D2_Db),
							((D2_Ei<<4)|D2_Eb),
							((D2_Fi<<4)|D2_Fb),
							((D2_Gi<<4)|D2_Gb)
						   }
};

static const uint8_t font_data_hexdigits[16] PROGMEM = { 
						NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7,
						NUM_8, NUM_9, NUM_A, NUM_B, NUM_C, NUM_D, NUM_E, NUM_F
};

void pcf8577_set_digit(uint8_t digit, uint8_t value) {
	uint8_t font_data, segment;
	uint8_t fb_idx, fb_bit;

	pcf8577_clr_digit(digit);

	/* leading 1 ? */
	if(digit == 3) {
		if(value)
			framebuffer[1] |= 0x04;
		return;
	}

	font_data = pgm_read_byte(&font_data_hexdigits[value]);
	for(segment = 0; segment < 7; segment++) {	// check all segments [ABCDEFG=0123456]
		fb_idx = (pgm_read_byte(&segtab[digit][segment]) >> 4) & 0x07;
		fb_bit = pgm_read_byte(&segtab[digit][segment]) & 0x07;

		if(font_data & (1<<segment)) {	// this segment is on for digit VALUE
			framebuffer[fb_idx] |= (1<<fb_bit);
		}		
	}

	return;
}

void pcf8577_set_sym(uint8_t n) {
	uint8_t fb_index = (pgm_read_byte(&symtab[n]) >> 4)&0x07;
	uint8_t fb_bit = pgm_read_byte(&symtab[n]) & 0x07;

	framebuffer[fb_index] |= (1<<fb_bit);
}

void pcf8577_set_dp(uint8_t n) {
	uint8_t fb_index = (pgm_read_byte(&dptab[n]) >> 4)&0x07;
	uint8_t fb_bit = pgm_read_byte(&dptab[n] ) & 0x07;

	framebuffer[fb_index] |= (1<<fb_bit);
}

void pcf8577_clr_dp(void) {
	framebuffer[0] &= ~(1<<2);
	framebuffer[4] &= ~((1<<5)|(1<<2));
}

void pcf8577_ss_putint(int i) {

}

void pcf8577_clr_digit(uint8_t n) {
	switch(n) {
		case 0:
			framebuffer[0] &= ~(0x80);
			framebuffer[1] &= ~(0x80);
			framebuffer[2] &= ~(1);
			framebuffer[3] &= ~((1<<0)|(1<<2));
			framebuffer[4] &= ~(2);
			framebuffer[5] &= ~(0x02); 
			break;
		case 1:
			framebuffer[0] &= ~(0x60);
			framebuffer[1] &= ~(0x60);
			framebuffer[4] &= ~(0x8);
			framebuffer[5] &= ~(0x0C);
			break;
		case 2:
			framebuffer[0] &= ~(0x18);
			framebuffer[1] &= ~(0x18);
			framebuffer[4] &= ~(0x40);
			framebuffer[5] &= ~(0x60);
			break;
		case 3:
			framebuffer[1] &= ~(4);
			break;
	}
}

void pcf8577_clr_ss(void) {
	pcf8577_clr_digit(0);	
	pcf8577_clr_digit(1);
	pcf8577_clr_digit(2);
	pcf8577_clr_digit(3);
}

void pcf8577_clr_symbols(void) {
	framebuffer[0] &= ~((1<<1));
	framebuffer[1] &= ~((1<<1));
	framebuffer[2] &= ~((1<<2)|(1<<3)|(1<<4)|(1<<5));
	framebuffer[3] &= ~((1<<3)|(1<<4)|(1<<5)|(1<<6));
	// 4 nothing
	framebuffer[5] &= ~((1<<4));
	framebuffer[6] &= ~(1|(1<<0x01));
	framebuffer[7] &= ~((1<<0x01));
}


//void pcf8577_time(void) {

//}

void pcf8577_setram(uint8_t pattern) {
	uint8_t i;
	for(i = 0; i < 8; i++) {
 		framebuffer[i] = pattern;
    }
}

void pcf8577_syncfb(void) {
	uint8_t i;
	for(i = 0; i < 8; i++) {
        	i2c_start_wait(0x74|I2C_WRITE);
        	i2c_write(i|0x80); // ramaddr | duplexmode
        	i2c_write(framebuffer[i]);
        	i2c_stop();
    }
}

