#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <inttypes.h>

extern volatile uint32_t clock_s;

#define clocktick()		clock_s++
#define get_clock_s()	clock_s
void init_clock(void);


#endif /// __CLOCK_H__
