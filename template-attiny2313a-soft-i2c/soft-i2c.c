#if 0


//----------Autor: R�diger Hambsch  Elektronikschule Tettnang---------
// Mai 2005
// �berarbeitet: Wilhelm Amann Mai 2007
/*##################  C51_Funktionen fuer I2C-Bus  ##################

  die Zurordnug von SDA und SCL muss vorher geschehen
  z.B.  
	sbit SDA=0x90;//P1.0
	sbit SCL=0x91;//P1.1
							*/
//-------------------------------------------------------------
void wait(unsigned char wert)
	{do{wert=wert-1;}while(wert!=0);}
//-------------------------------------------------------------
void i2c_bus_start()  	//abfallende Flanke  von SDA ,w�hrend SCL auf High
{
	SDA=1;
	wait(1);
    	SCL=1;
    	while(SCL==0);  	//warten falls ein Slave noch bremst
	wait(1);
	SDA=0;		    	//abfallende Flanke produziert Start-Condition
    	wait(1);
	SCL=0;
	wait(1);
}
//-------------------------------------------------------------
void i2c_bus_stop()		//ansteigende Flanke  von SDA ,w�hrend SCL auf High
{
	SDA=0;
	wait(1);
	SCL=1;
	while(SCL==0);  	//warten falls ein Slave noch bremst
    	wait(1);
	SDA=1; 				//ansteigende Flanke produziert Stop-Condition
	wait(1);
}
//--------------------------------------------------------------
void i2c_bit_write(bit bitwert)	//bitwert wird geschrieben
{
	SDA=bitwert;		//Bit-Wert nach  SDA �bernehmen
	wait(1);
	SCL=1;
    	while(SCL==0);  	//warten falls der slave ausbremst
	wait(1);
	SCL=0;
    	wait(1);
	SDA=0;
}
//-------------------------------------------------------------
bit i2c_bit_read()	//bitwert wird gelesen
{
	bit bitwert;
	SCL=0;
    	wait(5);
	SDA=1;
	wait(5);
	SCL=1;
	while(SCL==0);  	//warten falls Slave ausbremst
	wait(5);		//warten ca 5�s auf die Ver�nderung von SDA 
    	bitwert=SDA;    	//Zustand von SDA der Bitvariable  �bergeben
    	SCL=0;
    	wait(5);
	SDA=0;
	wait(5);
	return(bitwert);
	
	}
//-------------------------------------------------------------
void i2c_ack_slave()	//Quittung vom Slave an den Master
{
	SDA=1;
	wait(5);
	SCL=1;
	while(SCL==0);     	//warten falls ein Slave ausbremst
    	wait(5);
	while(SDA==1);  	//warten bis Acknowledge vom Slave kommt
	wait(5);
	SCL=0;
    	wait(5);
	SDA=0;
	wait(5);
}
//--------------------------------------------------------------
void i2c_ack_master()  //ACK-Quittung vom Master an den Slave
{
	SDA=0;
	wait(1);
	SCL=1;
	while(SCL==0);  //warten falls ein Slave ausbremst
    	wait(5);	//Zeit dass Slave vom Master das ACK  erfahren kann
	SCL=0;
    	wait(1);
	SDA=0;
}
//--------------------------------------------------------------
void i2c_nack_master()    //NACK-Quittung vom Master an den Slave
{						
	SDA=1;
	wait(1);
	SCL=1;
	while(SCL==0);     //warte falls ein Slave ausbremst
    	wait(5);
	SCL=0;
	wait(1);
	SDA=0;
}
//--------------------------------------------------------------
void i2c_byte_write (unsigned char wert)  //Das �bergebene Zeichen wird auf den I2C-Bus geschrieben
{
	unsigned char i;
	bit bitwert;
	for(i=0;i<=7;i++)
	{
	bitwert=(wert&0x80)/128;	//MSB herausfiltern
	i2c_bit_write(bitwert);
	wert=wert<<1;
	}
}
//--------------------------------------------------------------
unsigned char i2c_byte_read()	// ein Byte wird vom I2C-Bus gelesen 
{
	unsigned char i,wert=0;
   unsigned char eins;
   bit bitwert;
    eins=1;
	for(i=0;i<=7;i++)
	{
	 bitwert = i2c_bit_read();
	wert=  (unsigned char) bitwert * (eins <<(7-i))  + wert;
	}
	return(wert);
}

#endif
