#include <avr/io.h>
#include <util/delay.h>

int main() {
	// init IO
	DDRB = 0xff;
	DDRD = (1<<5);
	PORTD &= (1<<5);
	PORTB = ~((1<<4)|(1<<5));

	while (1) {
		PORTB ^= (1<<PB2);	// 2gn,3bl,4rt
		_delay_ms(25);
		PORTB = 0xFF;
		PORTB &= ~(1<<4);
		_delay_ms(35);
	}
}

